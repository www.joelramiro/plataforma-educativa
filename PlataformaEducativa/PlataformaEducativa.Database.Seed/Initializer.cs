﻿// <copyright file="Initializer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Seed
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.Extensions.DependencyInjection;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Context.Configuration;
    using PlataformaEducativa.Database.Model.People;

    public static class Initializer
    {
        public static async Task SeedAsync(IServiceProvider serviceProvider)
        {
            bool databaseCreated = false;
            bool databaseConfigurationCreated = false;
            try
            {
                databaseCreated = await serviceProvider.GetRequiredService<PlataformaEducativaContext>().Database.EnsureCreatedAsync();
                databaseConfigurationCreated = await serviceProvider.GetRequiredService<ConfigurationContext>().Database.EnsureCreatedAsync();
            }
            catch (Exception ex)
            {
                var message = ex.Message;
            }

            if (!databaseCreated)
            {
                return;
            }

            var context = serviceProvider.GetRequiredService<PlataformaEducativaContext>();

            await CreateSeed(context);
            /*try
            {
                await configurationContext.SaveChangesAsync();
            }
            finally
            {
                configurationContext.Database.CloseConnection();
            }*/
        }

        private static async Task CreateSeed(PlataformaEducativaContext context)
        {
            // Sex
            context.SexType.Add(new SexType { Value = "Hombre" });
            context.SexType.Add(new SexType { Value = "Mujer" });
            await context.SaveChangesAsync();

            // BloodType
            context.BloodType.Add(new BloodType { Type = "A+" });
            context.BloodType.Add(new BloodType { Type = "B+" });
            context.BloodType.Add(new BloodType { Type = "AB+" });
            context.BloodType.Add(new BloodType { Type = "O+" });
            context.BloodType.Add(new BloodType { Type = "A-" });
            context.BloodType.Add(new BloodType { Type = "B-" });
            context.BloodType.Add(new BloodType { Type = "AB-" });
            context.BloodType.Add(new BloodType { Type = "O-" });
            await context.SaveChangesAsync();

            // CivilStatus
            context.CivilStatus.Add(new CivilStatus { Description = "Soltero" });
            context.CivilStatus.Add(new CivilStatus { Description = "Casado" });
            context.CivilStatus.Add(new CivilStatus { Description = "Union Libre" });
            await context.SaveChangesAsync();

            // Country
            var Honduras = new Country { Name = "Honduras" };
            context.Country.Add(Honduras);
            await context.SaveChangesAsync();

            // EducationLevel
            context.EducationLevel.Add(new EducationLevel { Level = "Primaria" });
            context.EducationLevel.Add(new EducationLevel { Level = "Secundaria" });
            context.EducationLevel.Add(new EducationLevel { Level = "Profesional" });
            context.EducationLevel.Add(new EducationLevel { Level = "Pasante Universitario" });
            context.EducationLevel.Add(new EducationLevel { Level = "Universitario" });
            await context.SaveChangesAsync();

            // Nacionality
            context.Nacionality.Add(new Nacionality { IdCountry = Honduras.Id });
            await context.SaveChangesAsync();

            // PriorityLevel
            context.PriorityLevel.Add(new PriorityLevel { Level = "Bajo" });
            context.PriorityLevel.Add(new PriorityLevel { Level = "Medio" });
            context.PriorityLevel.Add(new PriorityLevel { Level = "Alto" });
            await context.SaveChangesAsync();

            // RelationTypes
            context.RelationType.Add(new RelationType { Name = "Abuelo" });
            context.RelationType.Add(new RelationType { Name = "Abuela" });
            context.RelationType.Add(new RelationType { Name = "Tio" });
            context.RelationType.Add(new RelationType { Name = "Tia" });
            context.RelationType.Add(new RelationType { Name = "Primo" });
            context.RelationType.Add(new RelationType { Name = "Prima" });
            context.RelationType.Add(new RelationType { Name = "Esposo" });
            context.RelationType.Add(new RelationType { Name = "Esposa" });
            context.RelationType.Add(new RelationType { Name = "Padre" });
            context.RelationType.Add(new RelationType { Name = "Madre" });
            context.RelationType.Add(new RelationType { Name = "Hermano" });
            context.RelationType.Add(new RelationType { Name = "Hermana" });
            context.RelationType.Add(new RelationType { Name = "Hijo" });
            context.RelationType.Add(new RelationType { Name = "Hija" });
            await context.SaveChangesAsync();
        }
    }
}