﻿namespace PlataformaEducativa.Security.Policies
{
    public static class PolicyNames
    {
        public const string CanViewReports = "CanViewReports";
        public const string CanEditReport = "CanEditReport";
        public const string CanDeleteReport = "CanDeleteReport";
        public const string CanCreateReport = "CanCreateReport";
        public const string CanPrintReport = "CanPrintReport";
    }
}
