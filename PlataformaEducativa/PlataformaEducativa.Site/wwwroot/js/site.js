﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

$('[data-bootstrap-date-picker="true"]')
    .each(function () {
        $(this).datepicker({
            autoclose: true,
            container: $(this),
            clearBtn: true,
            keepEmptyValues: true,
            language:'es'
        })
    });

$('[data-loader="true"]').on("click", function (e) { $('.page-loader-wrapper').fadeIn(); });

$.validator.unobtrusive.options = {
    highlight: function (input) {
        $(input).parents('.form-line').addClass('error');
    },
    unhighlight: function (input) {
        $(input).parents('.form-line').removeClass('error');
    },
    errorPlacement: function (error, element) {
        $(element).parents('.form-group').append(error);
    },
    errorElement: 'label',
    errorClass: 'error'
};

let textFieldTemplate = function () {
    if (!this.filtering)
        return "";
    var a = this._grid
    var control = $('<div class="header-group"><div class="form-line"><input class="form-control" /></div></div>');
    this.filterControl = control.find('input');
    if (this.autosearch) {
        this.filterControl.on("keypress", function (b) {
            13 === b.which && (a.search(),
                b.preventDefault())
        });
    }
    return control;
};


let dateTextFieldTemplate = function () {
    if (!this.filtering)
        return "";
    var a = this._grid

    var control = $('<div class="header-group"> <div class="input-group"><div class="form-line"><input class="form-control" /></div></div></div>');
    this.filterControl = control.find('input');
    let inputGroup = control.find('.input-group');

    this.filterControl.datepicker({
        autoclose: true,
        clearBtn: true,
        keepEmptyValues: true,
        language: 'es',
    })
        .on('show', function (e) {
            let top = $('.datepicker-dropdown').css('top');
            top = parseInt(top.replace(/(\d+)px/i, '$1')) + 108;
            $('.datepicker-dropdown').css('top', top + 'px')
            // `e` here contains the extra attributes
        });
    if (this.autosearch) {
        this.filterControl.on("keypress", function (b) {
            13 === b.which && (a.search(),
                b.preventDefault())
        });
    }
    return control;
};


let numberFieldTemplate = function () {
    if (!this.filtering)
        return "";
    var a = this._grid

    var control = $('<div class="header-group"><div class="form-line"><input type="number" class="form-control" /></div></div>');
    this.filterControl = control.find('input');
    if (this.autosearch) {
        this.filterControl.on("keypress", function (b) {
            13 === b.which && (a.search(),
                b.preventDefault())
        });
    }
    return control;
};

let selectTemplate = function () {
    if (!this.filtering)
        return "";
    var a = this._grid

    var control = $('<div class="header-group"><select class="form-control" data-container="body"></div>'),
        valueField = this.valueField,
        textField = this.textField,
        selectedIndex = this.selectedIndex;
    var input = control.find('select')
    $.each(this.items, function (index, item) {
        var value = valueField ? item[valueField] : index,
            text = textField ? item[textField] : item;

        $("<option>")
            .attr("value", value)
            .text(text)
            .appendTo(input);

    });

    control.prop("disabled", !!this.readOnly);
    control.prop("selectedIndex", selectedIndex);

    if (this.autosearch) {
        control.on("change", function (e) {
            grid.search();
        });
    }

    return control;
};
