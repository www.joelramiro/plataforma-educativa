$(function () {
    //Tooltip
    $('[data-tooltip="tooltip"]').tooltip({
        container: 'body'
    });

    //Popover
    $('[data-tooltip="popover"]').popover();
})