// <copyright file="Startup.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Site
{
    using EfCore.Triggers.Runner;
    using Hangfire;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using PlataformaEducativa.Core.People.People;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Context.Configuration;
    using PlataformaEducativa.Database.Model.Institute;
    using PlataformaEducativa.Database.Model.People;
    using PlataformaEducativa.Database.Model.Registration;
    using PlataformaEducativa.Database.Repository.Database.Institute;
    using PlataformaEducativa.Database.Repository.Database.People;
    using PlataformaEducativa.Database.Repository.Database.Registration;
    using PlataformaEducativa.Database.Repository.GeneralConfigurationResources;
    using Swashbuckle.AspNetCore.Swagger;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCloudscribeNavigation(this.Configuration.GetSection("NavigationOptions"));

            services.AddLocalization();

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddDbContext<ConfigurationContext>(options =>
            {
                options.UseSqlServer(this.Configuration.GetConnectionString("ConfigurationConnection"));
                options.EnableSensitiveDataLogging(true);
                options.EnableDetailedErrors(true);
            });

            services.AddDbContext<PlataformaEducativaContext>(options =>
            {
                options.UseSqlServer(this.Configuration.GetConnectionString("PlataformaEducativaConnection"));
                options.EnableSensitiveDataLogging(true);
                options.EnableDetailedErrors(true);
            });

            this.AddRepositories(services);
            this.AddManagers(services);

            services.AddScoped<ITriggerRunner, TriggerRunner>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Communication Channel Gateway", Version = "v1" });
            });
            services.AddHangfire(configuration => configuration
               .UseSimpleAssemblyNameTypeSerializer()
               .UseRecommendedSerializerSettings()
               .UseSqlServerStorage(this.Configuration.GetConnectionString("HangfireConnection")));

            // Add the processing server as IHostedService
            services.AddHangfireServer();
        }

        private void AddManagers(IServiceCollection services)
        {
            services.AddScoped<IPeopleManager, PeopleManager>();
        }

        private void AddRepositories(IServiceCollection services)
        {
            services.AddScoped<IRepository<Address>, AddressRepository>();
            services.AddScoped<IRepository<Allergy>, AllergyRepository>();
            services.AddScoped<IRepository<BloodType>, BloodTypeRepository>();
            services.AddScoped<IRepository<CivilStatus>, CivilStatusRepository>();
            services.AddScoped<IRepository<Country>, CountryRepository>();
            services.AddScoped<IRepository<Disability>, DisabilityRepository>();
            services.AddScoped<IRepository<Disease>, DiseaseRepository>();
            services.AddScoped<IRepository<EducationLevel>, EducationLevelRepository>();
            services.AddScoped<IRepository<Email>, EmailRepository>();
            services.AddScoped<IRepository<HealthInsurance>, HealthInsuranceRepository>();
            services.AddScoped<IRepository<HealthInsuranceAddress>, HealthInsuranceAddressRepository>();
            services.AddScoped<IRepository<HealthInsuranceEmail>, HealthInsuranceEmailRepository>();
            services.AddScoped<IRepository<HealthInsuranceTelephone>, HealthInsuranceTelephoneRepository>();
            services.AddScoped<IRepository<Nacionality>, NacionalityRepository>();
            services.AddScoped<IRepository<Database.Model.People.People>, PeopleRepository>();
            services.AddScoped<IRepository<PeopleAddress>, PeopleAddressRepository>();
            services.AddScoped<IRepository<PeopleAllergy>, PeopleAllergyRepository>();
            services.AddScoped<IRepository<PeopleDisability>, PeopleDisabilityRepository>();
            services.AddScoped<IRepository<PeopleDisease>, PeopleDiseaseRepository>();
            services.AddScoped<IRepository<PeopleEmail>, PeopleEmailRepository>();
            services.AddScoped<IRepository<PeopleNacionality>, PeopleNacionalityRepository>();
            services.AddScoped<IRepository<PeopleTelephone>, PeopleTelephoneRepository>();
            services.AddScoped<IRepository<PriorityLevel>, PriorityLevelRepository>();
            services.AddScoped<IRepository<Relation>, RelationRepository>();
            services.AddScoped<IRepository<RelationType>, RelationTypeRepository>();
            services.AddScoped<IRepository<SexType>, SexTypeRepository>();
            services.AddScoped<IRepository<Telephone>, TelephoneRepository>();
            services.AddScoped<IRepository<Building>, BuildingRepository>();
            services.AddScoped<IRepository<Campus>, CampusRepository>();
            services.AddScoped<IRepository<Classroom>, ClassroomRepository>();
            services.AddScoped<IRepository<Institution>, InstitutionRepository>();
            services.AddScoped<IRepository<Career>, CareerRepository>();
            services.AddScoped<IRepository<CareerCampus>, CareerCampusRepository>();
            services.AddScoped<IRepository<CareerClass>, CareerClassRepository>();
            services.AddScoped<IRepository<CareerStudent>, CareerStudentRepository>();
            services.AddScoped<IRepository<Class>, ClassRepository>();
            services.AddScoped<IRepository<Period>, PeriodRepository>();
            services.AddScoped<IRepository<Registration>, RegistrationRepository>();
            services.AddScoped<IRepository<RegistrationDetail>, RegistrationDetailRepository>();
            services.AddScoped<IRepository<Schedule>, ScheduleRepository>();
            services.AddScoped<IRepository<SectionClass>, SectionClassRepository>();
            services.AddScoped<IRepository<Student>, StudentRepository>();
            services.AddScoped<IRepository<Teacher>, TeacherRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Communication Channel Gateway V1");
            });

            app.UseHangfireDashboard();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
