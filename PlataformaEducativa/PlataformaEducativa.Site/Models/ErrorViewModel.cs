// <copyright file="ErrorViewModel.cs" company="Leitz Group">
// Copyright (c) Leitz Group. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Site.Models
{
    public class ErrorViewModel
    {
        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(this.RequestId);
    }
}