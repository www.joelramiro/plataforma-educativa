﻿// <copyright file="InstitutionListViewModel.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Site.Models.Institution
{
    public class InstitutionListViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Moto { get; set; }
    }
}
