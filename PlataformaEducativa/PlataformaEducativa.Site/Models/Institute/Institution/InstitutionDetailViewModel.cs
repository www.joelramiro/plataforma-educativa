﻿// <copyright file="InstitutionDetailViewModel.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Site.Models.Institution
{
    public class InstitutionDetailViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Moto { get; set; }

        public string Logo { get; set; }
    }
}
