﻿// <copyright file="InstitutionEditViewModel.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Site.Models.Institution
{
    using Microsoft.AspNetCore.Http;

    public class InstitutionEditViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Moto { get; set; }

        public IFormFile Logo { get; set; }
    }
}
