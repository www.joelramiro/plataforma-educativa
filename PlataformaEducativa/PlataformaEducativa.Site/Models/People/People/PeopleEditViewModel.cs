﻿// <copyright file="PeopleEditViewModel.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Site.Models.People
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using PlataformaEducativa.Database.Model.People;

    public class PeopleEditViewModel
    {
        public int Id { get; set; }

        [Display(Name = nameof(Identity), ResourceType = typeof(Resources.Model.People.People.Edit))]
        public string Identity { get; set; }

        [Display(Name = nameof(FirstName), ResourceType = typeof(Resources.Model.People.People.Edit))]
        public string FirstName { get; set; }

        [Display(Name = nameof(SecondName), ResourceType = typeof(Resources.Model.People.People.Edit))]
        public string SecondName { get; set; }

        [Display(Name = nameof(MiddleName), ResourceType = typeof(Resources.Model.People.People.Edit))]
        public string MiddleName { get; set; }

        [Display(Name = nameof(LastName), ResourceType = typeof(Resources.Model.People.People.Edit))]
        public string LastName { get; set; }

        [Display(Name = nameof(BirthDate), ResourceType = typeof(Resources.Model.People.People.Edit))]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime BirthDate { get; set; }

        [Display(Name = nameof(EducationLevelId), ResourceType = typeof(Resources.Model.People.People.Edit))]
        public int EducationLevelId { get; set; }

        [Display(Name = nameof(SexTypeId), ResourceType = typeof(Resources.Model.People.People.Edit))]
        public int SexTypeId { get; set; }

        [Display(Name = nameof(CivilStatusId), ResourceType = typeof(Resources.Model.People.People.Edit))]
        public int CivilStatusId { get; set; }

        [Display(Name = nameof(CivilStatuses), ResourceType = typeof(Resources.Model.People.People.Edit))]
        public IEnumerable<CivilStatus> CivilStatuses { get; set; }

        [Display(Name = nameof(SexTypes), ResourceType = typeof(Resources.Model.People.People.Edit))]
        public IEnumerable<SexType> SexTypes { get; set; }

        [Display(Name = nameof(EducationLevels), ResourceType = typeof(Resources.Model.People.People.Edit))]
        public IEnumerable<EducationLevel> EducationLevels { get; set; }
    }
}
