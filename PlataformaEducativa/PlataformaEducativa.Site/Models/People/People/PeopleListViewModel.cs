﻿// <copyright file="PeopleListViewModel.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Site.Models.People
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class PeopleListViewModel
    {
        public int Id { get; set; }

        [Display(Name = nameof(Identity), ResourceType = typeof(Resources.Model.People.People.List))]
        public string Identity { get; set; }

        [Display(Name = nameof(FullName), ResourceType = typeof(Resources.Model.People.People.List))]
        public string FullName { get; set; }

        [Display(Name = nameof(BirthDate), ResourceType = typeof(Resources.Model.People.People.List))]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime BirthDate { get; set; }

        [Display(Name = nameof(EducationLevel), ResourceType = typeof(Resources.Model.People.People.List))]
        public string EducationLevel { get; set; }

        [Display(Name = nameof(SexType), ResourceType = typeof(Resources.Model.People.People.List))]
        public string SexType { get; set; }

        [Display(Name = nameof(CivilStatus), ResourceType = typeof(Resources.Model.People.People.List))]
        public string CivilStatus { get; set; }
    }
}
