﻿// <copyright file="PeopleController.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Site.Controllers
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using PlataformaEducativa.Core.People.People;
    using PlataformaEducativa.Database.Model.People;
    using PlataformaEducativa.Database.Repository.GeneralConfigurationResources;
    using PlataformaEducativa.Site.Models.People;

    public class PeopleController : Controller
    {
        private readonly IPeopleManager peopleManager;
        private readonly IRepository<CivilStatus> civilStatusRepository;
        private readonly IRepository<SexType> sexTypeRepository;
        private readonly IRepository<EducationLevel> educationLevelRepository;

        public PeopleController(
            IPeopleManager peopleManager,
            IRepository<CivilStatus> civilStatusRepository,
            IRepository<SexType> sexTypeRepository,
            IRepository<EducationLevel> educationLevelRepository)
        {
            this.peopleManager = peopleManager;
            this.civilStatusRepository = civilStatusRepository;
            this.sexTypeRepository = sexTypeRepository;
            this.educationLevelRepository = educationLevelRepository;
        }

        public async Task<IActionResult> Index()
        {
            var list = (await this.peopleManager.GetAllAsync()).Select(p => new PeopleListViewModel
            {
                Id = p.Id,
                FullName = $"{p.FirstName} {p.SecondName} {p.MiddleName} {p.LastName}",
                BirthDate = p.BirthDate,
                CivilStatus = p.CivilStatus.Description,
                EducationLevel = p.EducationLevel.Level,
                Identity = p.Identity,
                SexType = p.SexType.Value,
            }).ToList();

            return this.View(list);
        }

        public async Task<IActionResult> Create()
        {
            return this.View(new PeopleCreateViewModel
            {
                CivilStatuses = await this.civilStatusRepository.All().ToListAsync(),
                SexTypes = await this.sexTypeRepository.All().ToListAsync(),
                EducationLevels = await this.educationLevelRepository.All().ToListAsync(),
                BirthDate = DateTime.Now
            });
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return this.BadRequest();
            }

            var people = await this.peopleManager.GetByIdAsync(id.Value);

            if (people == null)
            {
                return this.BadRequest();
            }

            return this.View(new PeopleDetailViewModel
            {
                Id = people.Id,
                SexType = people.SexType.Value,
                BirthDate = people.BirthDate,
                CivilStatus = people.CivilStatus.Description,
                EducationLevel = people.EducationLevel.Level,
                Identity = people.Identity,
                FullName = $"{people.FirstName} {people.SecondName} {people.MiddleName} {people.LastName}",
            });
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return this.BadRequest();
            }

            var people = await this.peopleManager.GetByIdAsync(id.Value);

            if (people == null)
            {
                return this.BadRequest();
            }

            var result = await this.peopleManager.DeleteAsync(id.Value);

            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(validations.Key, item);
                    }
                }

                return this.RedirectToAction("Index");
            }

            return this.RedirectToAction("Index");
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return this.BadRequest();
            }

            var people = await this.peopleManager.GetByIdAsync(id.Value);

            if (people == null)
            {
                return this.BadRequest();
            }

            return this.View(new PeopleEditViewModel
            {
                SexTypeId = people.IdSexType,
                CivilStatusId = people.IdCivilStatus,
                EducationLevelId = people.IdEducationLevel,
                CivilStatuses = await this.civilStatusRepository.All().ToListAsync(),
                SexTypes = await this.sexTypeRepository.All().ToListAsync(),
                EducationLevels = await this.educationLevelRepository.All().ToListAsync(),
                Id = people.Id,
                BirthDate = people.BirthDate,
                FirstName = people.FirstName,
                SecondName = people.SecondName,
                MiddleName = people.MiddleName,
                LastName = people.LastName,
                Identity = people.Identity,
            });
        }

        [HttpPost]
        public async Task<IActionResult> Edit(PeopleEditViewModel peopleEditViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                peopleEditViewModel.CivilStatuses = await this.civilStatusRepository.All().ToListAsync();
                peopleEditViewModel.SexTypes = await this.sexTypeRepository.All().ToListAsync();
                peopleEditViewModel.EducationLevels = await this.educationLevelRepository.All().ToListAsync();
                return this.View(peopleEditViewModel);
            }

            var people = await this.peopleManager.GetByIdAsync(peopleEditViewModel.Id);

            if (people == null)
            {
                return this.BadRequest();
            }

            people.IdEducationLevel = peopleEditViewModel.EducationLevelId;
            people.BirthDate = peopleEditViewModel.BirthDate;
            people.IdCivilStatus = peopleEditViewModel.CivilStatusId;
            people.FirstName = peopleEditViewModel.FirstName;
            people.SecondName = peopleEditViewModel.SecondName;
            people.MiddleName = peopleEditViewModel.MiddleName;
            people.LastName = peopleEditViewModel.LastName;
            people.Identity = peopleEditViewModel.Identity;
            people.IdSexType = peopleEditViewModel.SexTypeId;

            var result = await this.peopleManager.EditAsync(people);

            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(validations.Key, item);
                    }
                }

                peopleEditViewModel.CivilStatuses = await this.civilStatusRepository.All().ToListAsync();
                peopleEditViewModel.SexTypes = await this.sexTypeRepository.All().ToListAsync();
                peopleEditViewModel.EducationLevels = await this.educationLevelRepository.All().ToListAsync();
                return this.View(peopleEditViewModel);
            }

            return this.RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> Create(PeopleCreateViewModel peopleCreateViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                peopleCreateViewModel.CivilStatuses = await this.civilStatusRepository.All().ToListAsync();
                peopleCreateViewModel.SexTypes = await this.sexTypeRepository.All().ToListAsync();
                peopleCreateViewModel.EducationLevels = await this.educationLevelRepository.All().ToListAsync();
                return this.View(peopleCreateViewModel);
            }

            var result = await this.peopleManager.CreateAsync(
                new Database.Model.People.People
                {
                    IdEducationLevel = peopleCreateViewModel.EducationLevelId,
                    BirthDate = peopleCreateViewModel.BirthDate,
                    IdCivilStatus = peopleCreateViewModel.CivilStatusId,
                    FirstName = peopleCreateViewModel.FirstName,
                    SecondName = peopleCreateViewModel.SecondName,
                    MiddleName = peopleCreateViewModel.MiddleName,
                    LastName = peopleCreateViewModel.LastName,
                    Identity = peopleCreateViewModel.Identity,
                    IdSexType = peopleCreateViewModel.SexTypeId,
                });

            if (!result.Succeeded)
            {
                foreach (var validations in result.ValidationMessages)
                {
                    foreach (var item in validations.Value)
                    {
                        this.ModelState.AddModelError(validations.Key, item);
                    }
                }

                peopleCreateViewModel.CivilStatuses = await this.civilStatusRepository.All().ToListAsync();
                peopleCreateViewModel.SexTypes = await this.sexTypeRepository.All().ToListAsync();
                peopleCreateViewModel.EducationLevels = await this.educationLevelRepository.All().ToListAsync();
                return this.View(peopleCreateViewModel);
            }

            return this.RedirectToAction("Index");
        }
    }
}