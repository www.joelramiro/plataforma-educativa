﻿// <copyright file="IPeopleManager.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Core.People.People
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using global::People.Core;

    public interface IPeopleManager
    {
        Task<OperationResult> CreateAsync(Database.Model.People.People people);

        Task<OperationResult> EditAsync(Database.Model.People.People people);

        Task<OperationResult> DeleteAsync(int? idPeople);

        Task<IEnumerable<Database.Model.People.People>> GetAllAsync();

        Task<Database.Model.People.People> GetByIdAsync(int? idPeople);
    }
}
