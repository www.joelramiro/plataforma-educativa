﻿// <copyright file="PeopleManager.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Core.People.People
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using global::People.Core;
    using Microsoft.EntityFrameworkCore;
    using PlataformaEducativa.Database.Model.People;
    using PlataformaEducativa.Database.Repository.GeneralConfigurationResources;

    public class PeopleManager : IPeopleManager
    {
        private readonly IRepository<Database.Model.People.People> peopleRepository;

        public PeopleManager(IRepository<Database.Model.People.People> peopleRepository)
        {
            this.peopleRepository = peopleRepository;
        }

        public async Task<OperationResult> CreateAsync(Database.Model.People.People people)
        {
            if (people == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(people)] = new string[] { "El objeto persona no puede ser nulo." },
                    });
            }

            return await this.InnerCreateAsync(people);
        }

        private async Task<OperationResult> InnerCreateAsync(Database.Model.People.People people)
        {
            var exist = await this.peopleRepository.All().AnyAsync(p => p.Identity == people.Identity);
            if (exist)
            {
                return new OperationResult(
                       new Dictionary<string, IEnumerable<string>>()
                       {
                           [nameof(people.Identity)] = new string[] { $"Ya existe una persona con el número de identidad {people.Identity}." },
                       });
            }

            this.peopleRepository.Create(people);
            await this.peopleRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<OperationResult> DeleteAsync(int? idPeople)
        {
            if (idPeople == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(idPeople)] = new string[] { "El id no puede ser nulo." },
                    });
            }

            var people = await this.GetByIdAsync(idPeople.Value);

            if (people == null)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(people)] = new string[] { $"No se encontró una persona con el id {idPeople.Value}." },
                    });
            }

            this.peopleRepository.Delete(people);
            await this.peopleRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<OperationResult> EditAsync(Database.Model.People.People people)
        {
            var exist = await this.peopleRepository.All().AnyAsync(p => p.Identity == people.Identity && p.Id != people.Id);

            if (exist)
            {
                return new OperationResult(
                    new Dictionary<string, IEnumerable<string>>()
                    {
                        [nameof(people.Identity)] = new string[] { $"Ya existe una persona con el número de identidad {people.Identity}." },
                    });
            }

            this.peopleRepository.Update(people);
            await this.peopleRepository.SaveChangesAsync();
            return new OperationResult(true);
        }

        public async Task<IEnumerable<Database.Model.People.People>> GetAllAsync()
        {
            var list = await this.peopleRepository.All()
                .Include(p => p.EducationLevel)
                .Include(p => p.CivilStatus)
                .Include(p => p.SexType)
                .ToListAsync();
            return list;
        }

        public async Task<Database.Model.People.People> GetByIdAsync(int? idPeople)
        {
            if (idPeople == null)
            {
                return null;
            }

            return await this.peopleRepository.All()
                .Include(p => p.EducationLevel)
                .Include(p => p.CivilStatus)
                .Include(p => p.SexType)
                .FirstOrDefaultAsync(p => p.Id == idPeople.Value);
        }
    }
}
