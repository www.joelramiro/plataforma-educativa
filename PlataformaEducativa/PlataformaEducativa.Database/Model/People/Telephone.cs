﻿// <copyright file="Telephone.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Model.People
{
    public class Telephone
    {
        public int Id { get; set; }

        public string Number { get; set; }
    }
}
