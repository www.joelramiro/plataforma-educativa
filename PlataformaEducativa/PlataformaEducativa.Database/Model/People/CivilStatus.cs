﻿// <copyright file="CivilStatus.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Model.People
{
    public class CivilStatus
    {
        public int Id { get; set; }

        public string Description { get; set; }
    }
}
