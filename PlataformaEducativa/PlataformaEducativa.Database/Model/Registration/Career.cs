﻿// <copyright file="Career.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Model.Registration
{
    using System.Collections.Generic;

    public class Career
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public ICollection<CareerCampus> CareerCampuses { get; set; }

        public ICollection<CareerClass> CareerClasses { get; set; }

        public ICollection<CareerStudent> CareerStudents { get; set; }
    }
}
