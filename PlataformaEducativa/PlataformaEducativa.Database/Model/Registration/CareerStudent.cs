﻿// <copyright file="CareerStudent.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Model.Registration
{
    using System.ComponentModel.DataAnnotations.Schema;

    public class CareerStudent
    {
        public int Id { get; set; }

        public int IdStudent { get; set; }

        public int IdCareer { get; set; }

        [ForeignKey(nameof(IdCareer))]
        public Career Career { get; set; }

        [ForeignKey(nameof(IdStudent))]
        public Student Student { get; set; }
    }
}
