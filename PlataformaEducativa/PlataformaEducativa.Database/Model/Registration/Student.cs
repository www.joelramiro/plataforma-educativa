﻿// <copyright file="Student.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Model.Registration
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Student
    {
        public int Id { get; set; }

        public int IdPeople { get; set; }

        [ForeignKey(nameof(IdPeople))]
        public People.People People { get; set; }

        public ICollection<CareerStudent> CareerStudents { get; set; }
    }
}
