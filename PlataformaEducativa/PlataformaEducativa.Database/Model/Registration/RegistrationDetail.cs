﻿// <copyright file="RegistrationDetail.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Model.Registration
{
    using System.ComponentModel.DataAnnotations.Schema;

    public class RegistrationDetail
    {
        public int Id { get; set; }

        public int IdRegistration { get; set; }

        [ForeignKey(nameof(IdRegistration))]
        public Registration Registration { get; set; }

        public int IdSectionClass { get; set; }

        [ForeignKey(nameof(IdSectionClass))]
        public SectionClass SectionClass { get; set; }
    }
}
