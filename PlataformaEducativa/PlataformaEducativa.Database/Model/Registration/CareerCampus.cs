﻿// <copyright file="CareerCampus.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Model.Registration
{
    using System.ComponentModel.DataAnnotations.Schema;
    using PlataformaEducativa.Database.Model.Institute;

    public class CareerCampus
    {
        public int Id { get; set; }

        public int IdCareer { get; set; }

        public int IdCampus { get; set; }

        [ForeignKey(nameof(IdCampus))]
        public Campus Campus { get; set; }

        [ForeignKey(nameof(IdCareer))]
        public Career Career { get; set; }
    }
}
