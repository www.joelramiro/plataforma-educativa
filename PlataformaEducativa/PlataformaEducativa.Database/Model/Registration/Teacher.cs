﻿// <copyright file="Teacher.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Model.Registration
{
    using System.ComponentModel.DataAnnotations.Schema;

    public class Teacher
    {
        public int Id { get; set; }

        public int IdPeople { get; set; }

        [ForeignKey(nameof(IdPeople))]
        public People.People People { get; set; }
    }
}
