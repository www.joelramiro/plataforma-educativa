﻿// <copyright file="Schedule.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Model.Registration
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Schedule
    {
        public int Id { get; set; }

        public int IdSectionClass { get; set; }

        [ForeignKey(nameof(IdSectionClass))]
        public SectionClass SectionClass { get; set; }

        public DateTime DateTime { get; set; }

        public double DurationInMinutes { get; set; }
    }
}
