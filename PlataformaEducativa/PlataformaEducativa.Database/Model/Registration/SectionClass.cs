﻿// <copyright file="SectionClass.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Model.Registration
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using PlataformaEducativa.Database.Model.Institute;

    public class SectionClass
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int IdClass { get; set; }

        public int IdTeacher { get; set; }

        public int IdClassRoom { get; set; }

        [ForeignKey(nameof(IdClass))]
        public Class Class { get; set; }

        [ForeignKey(nameof(IdTeacher))]
        public Teacher Teacher { get; set; }

        [ForeignKey(nameof(IdClassRoom))]
        public Classroom Classroom { get; set; }

        public ICollection<RegistrationDetail> RegistrationDetails { get; set; }
    }
}
