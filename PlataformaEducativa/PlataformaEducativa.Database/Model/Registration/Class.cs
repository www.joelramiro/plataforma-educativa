﻿// <copyright file="Class.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Model.Registration
{
    using System.Collections.Generic;

    public class Class
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int RatingUnits { get; set; }

        public ICollection<CareerClass> CareerClasses { get; set; }
    }
}
