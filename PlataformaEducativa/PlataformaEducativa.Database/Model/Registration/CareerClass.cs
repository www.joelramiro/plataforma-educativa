﻿// <copyright file="CareerClass.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Model.Registration
{
    using System.ComponentModel.DataAnnotations.Schema;

    public class CareerClass
    {
        public int Id { get; set; }

        public int IdCareer { get; set; }

        public int IdClass { get; set; }

        [ForeignKey(nameof(IdCareer))]
        public Career Career { get; set; }

        [ForeignKey(nameof(IdClass))]
        public Class Class { get; set; }
    }
}