﻿// <copyright file="Period.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Model.Registration
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using PlataformaEducativa.Database.Model.Institute;

    public class Period
    {
        public int Id { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int IdCampus { get; set; }

        [ForeignKey(nameof(IdCampus))]
        public Campus Campus { get; set; }
    }
}
