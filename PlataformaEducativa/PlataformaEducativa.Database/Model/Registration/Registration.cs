﻿// <copyright file="Registration.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Model.Registration
{
    using System.ComponentModel.DataAnnotations.Schema;

    public class Registration
    {
        public int Id { get; set; }

        public int IdStudent { get; set; }

        public int IdPeriod { get; set; }

        [ForeignKey(nameof(IdStudent))]
        public Student Student { get; set; }

        [ForeignKey(nameof(IdPeriod))]
        public Period Period { get; set; }
    }
}
