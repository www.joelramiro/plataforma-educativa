﻿// <copyright file="ConfigurationPropertyDetail.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Model.Configuration
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class ConfigurationPropertyDetail
    {
        public long ConfigurationPropertyDetailId { get; set; }

        [Required]
        [StringLength(100)]
        public string ConfigurationKey { get; set; }

        public long ConfigurationPropertyId { get; set; }

        [StringLength(2000)]
        public string StringValue { get; set; }

        public decimal? NumericValue { get; set; }

        public DateTime? DateTimeValue { get; set; }

        public bool? BoolValue { get; set; }

        public virtual ConfigurationProperty Header { get; set; }
    }
}
