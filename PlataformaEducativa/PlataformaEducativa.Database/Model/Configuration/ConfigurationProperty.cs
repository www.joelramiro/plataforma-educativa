﻿// <copyright file="ConfigurationProperty.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Model.Configuration
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class ConfigurationProperty
    {
        public long ConfigurationPropertyId { get; set; }

        [Required]
        [StringLength(100)]
        public string ConfigurationKey { get; set; }

        [StringLength(2000)]
        public string StringValue { get; set; }

        public decimal? NumericValue { get; set; }

        public DateTime? DateTimeValue { get; set; }

        public bool? BoolValue { get; set; }

        public virtual ICollection<ConfigurationPropertyDetail> Details { get; set; }

        public static class WellKnowsKeys
        {
            public const string DefaultConnection = "test.com";
        }
    }
}
