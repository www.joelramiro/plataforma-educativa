﻿// <copyright file="Classroom.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Model.Institute
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using PlataformaEducativa.Database.Model.Registration;

    public class Classroom
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int Capacity { get; set; }

        [ForeignKey(nameof(IdBuilding))]
        public Building Building { get; set; }

        public int IdBuilding { get; set; }

        public ICollection<SectionClass> SectionClasses { get; set; }
    }
}
