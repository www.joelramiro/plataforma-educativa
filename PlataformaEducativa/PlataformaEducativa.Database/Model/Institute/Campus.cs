﻿// <copyright file="Campus.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Model.Institute
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using PlataformaEducativa.Database.Model.Registration;

    public class Campus
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public ICollection<Building> Buildings { get; set; }

        public ICollection<Period> Periods { get; set; }

        public ICollection<CareerCampus> CareerCampuses { get; set; }

        [ForeignKey(nameof(IdInstitution))]
        public Institution Institution { get; set; }

        public int IdInstitution { get; set; }
    }
}
