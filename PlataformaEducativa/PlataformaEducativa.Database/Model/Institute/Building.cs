﻿// <copyright file="Building.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Model.Institute
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Building
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public ICollection<Classroom> Classrooms { get; set; }

        public int IdCampus { get; set; }

        [ForeignKey(nameof(IdCampus))]
        public Campus Campus { get; set; }
    }
}
