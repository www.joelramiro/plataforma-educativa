﻿// <copyright file="Institution.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Model.Institute
{
    using System.Collections.Generic;

    public class Institution
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Moto { get; set; }

        public string Logo { get; set; }

        public ICollection<Campus> Campuses { get; set; }
    }
}
