﻿// <copyright file="PlataformaEducativaConfigurationDbContextRepositoryBase.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database
{
    using PlataformaEducativa.Database.Context.Configuration;
    using PlataformaEducativa.Database.Repository.GeneralConfigurationResources;

    public abstract class PlataformaEducativaConfigurationDbContextRepositoryBase<TEntity> : RepositoryBase<TEntity, ConfigurationContext>
        where TEntity : class
    {
        public PlataformaEducativaConfigurationDbContextRepositoryBase(ConfigurationContext context)
            : base(context)
        {
            this.Context = context;
        }
    }
}
