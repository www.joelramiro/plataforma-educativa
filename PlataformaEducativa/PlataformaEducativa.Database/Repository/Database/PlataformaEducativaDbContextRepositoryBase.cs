﻿// <copyright file="PlataformaEducativaDbContextRepositoryBase.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database
{
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Repository.GeneralConfigurationResources;

    public abstract class PlataformaEducativaDbContextRepositoryBase<TEntity> : RepositoryBase<TEntity, PlataformaEducativaContext>
        where TEntity : class
    {
        public PlataformaEducativaDbContextRepositoryBase(PlataformaEducativaContext context)
            : base(context)
        {
            this.Context = context;
        }
    }
}
