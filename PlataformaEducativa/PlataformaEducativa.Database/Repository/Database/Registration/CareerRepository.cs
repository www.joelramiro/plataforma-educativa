﻿// <copyright file="CareerRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.Registration
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.Registration;

    public class CareerRepository : PlataformaEducativaDbContextRepositoryBase<Career>
    {
        public CareerRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<Career> All()
        {
            return this.Context.Career;
        }


        protected override Career MapNewValuesToOld(Career oldEntity, Career newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
