﻿// <copyright file="SectionClassRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.Registration
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.Registration;

    public class SectionClassRepository : PlataformaEducativaDbContextRepositoryBase<SectionClass>
    {
        public SectionClassRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<SectionClass> All()
        {
            return this.Context.SectionClass;
        }


        protected override SectionClass MapNewValuesToOld(SectionClass oldEntity, SectionClass newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
