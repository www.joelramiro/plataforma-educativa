﻿// <copyright file="StudentRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.Registration
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.Registration;

    public class StudentRepository : PlataformaEducativaDbContextRepositoryBase<Student>
    {
        public StudentRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<Student> All()
        {
            return this.Context.Student;
        }


        protected override Student MapNewValuesToOld(Student oldEntity, Student newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
