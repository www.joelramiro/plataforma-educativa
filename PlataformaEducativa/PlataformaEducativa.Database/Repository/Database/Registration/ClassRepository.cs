﻿// <copyright file="ClassRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.Registration
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.Registration;

    public class ClassRepository : PlataformaEducativaDbContextRepositoryBase<Class>
    {
        public ClassRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<Class> All()
        {
            return this.Context.Class;
        }


        protected override Class MapNewValuesToOld(Class oldEntity, Class newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
