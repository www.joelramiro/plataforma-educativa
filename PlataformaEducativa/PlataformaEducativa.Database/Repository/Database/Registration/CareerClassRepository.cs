﻿// <copyright file="CareerClassRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.Registration
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.Registration;

    public class CareerClassRepository : PlataformaEducativaDbContextRepositoryBase<CareerClass>
    {
        public CareerClassRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<CareerClass> All()
        {
            return this.Context.CareerClass;
        }


        protected override CareerClass MapNewValuesToOld(CareerClass oldEntity, CareerClass newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
