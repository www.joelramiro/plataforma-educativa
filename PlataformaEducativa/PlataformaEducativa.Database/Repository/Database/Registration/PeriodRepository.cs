﻿// <copyright file="PeriodRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.Registration
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.Registration;

    public class PeriodRepository : PlataformaEducativaDbContextRepositoryBase<Period>
    {
        public PeriodRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<Period> All()
        {
            return this.Context.Period;
        }


        protected override Period MapNewValuesToOld(Period oldEntity, Period newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
