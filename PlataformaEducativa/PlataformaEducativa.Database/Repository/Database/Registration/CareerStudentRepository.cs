﻿// <copyright file="CareerStudentRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.Registration
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.Registration;

    public class CareerStudentRepository : PlataformaEducativaDbContextRepositoryBase<CareerStudent>
    {
        public CareerStudentRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<CareerStudent> All()
        {
            return this.Context.CareerStudent;
        }


        protected override CareerStudent MapNewValuesToOld(CareerStudent oldEntity, CareerStudent newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
