﻿// <copyright file="RegistrationRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.Registration
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.Registration;

    public class RegistrationRepository : PlataformaEducativaDbContextRepositoryBase<Model.Registration.Registration>
    {
        public RegistrationRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<Model.Registration.Registration> All()
        {
            return this.Context.Registration;
        }


        protected override Model.Registration.Registration MapNewValuesToOld(Model.Registration.Registration oldEntity, Model.Registration.Registration newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
