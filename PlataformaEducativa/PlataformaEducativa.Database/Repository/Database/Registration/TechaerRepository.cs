﻿// <copyright file="TeacherRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.Registration
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.Registration;

    public class TeacherRepository : PlataformaEducativaDbContextRepositoryBase<Teacher>
    {
        public TeacherRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<Teacher> All()
        {
            return this.Context.Teacher;
        }


        protected override Teacher MapNewValuesToOld(Teacher oldEntity, Teacher newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
