﻿// <copyright file="CareerCampusRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.Registration
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.Registration;

    public class CareerCampusRepository : PlataformaEducativaDbContextRepositoryBase<CareerCampus>
    {
        public CareerCampusRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<CareerCampus> All()
        {
            return this.Context.CareerCampus;
        }


        protected override CareerCampus MapNewValuesToOld(CareerCampus oldEntity, CareerCampus newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
