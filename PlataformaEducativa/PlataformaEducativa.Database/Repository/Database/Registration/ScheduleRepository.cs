﻿// <copyright file="ScheduleRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.Registration
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.Registration;

    public class ScheduleRepository : PlataformaEducativaDbContextRepositoryBase<Schedule>
    {
        public ScheduleRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<Schedule> All()
        {
            return this.Context.Schedule;
        }


        protected override Schedule MapNewValuesToOld(Schedule oldEntity, Schedule newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
