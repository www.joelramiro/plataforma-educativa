﻿// <copyright file="RegistrationDetailRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.Registration
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.Registration;

    public class RegistrationDetailRepository : PlataformaEducativaDbContextRepositoryBase<RegistrationDetail>
    {
        public RegistrationDetailRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<RegistrationDetail> All()
        {
            return this.Context.RegistrationDetail;
        }


        protected override RegistrationDetail MapNewValuesToOld(RegistrationDetail oldEntity, RegistrationDetail newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
