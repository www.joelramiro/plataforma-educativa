﻿// <copyright file="ClassroomRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.Institute
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.Institute;

    public class ClassroomRepository : PlataformaEducativaDbContextRepositoryBase<Classroom>
    {
        public ClassroomRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<Classroom> All()
        {
            return this.Context.Classroom;
        }


        protected override Classroom MapNewValuesToOld(Classroom oldEntity, Classroom newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
