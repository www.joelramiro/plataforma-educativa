﻿// <copyright file="InstitutionRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.Institute
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.Institute;

    public class InstitutionRepository : PlataformaEducativaDbContextRepositoryBase<Institution>
    {
        public InstitutionRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<Institution> All()
        {
            return this.Context.Institution;
        }


        protected override Institution MapNewValuesToOld(Institution oldEntity, Institution newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
