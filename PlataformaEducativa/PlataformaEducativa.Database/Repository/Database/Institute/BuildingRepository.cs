﻿// <copyright file="BuildingRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.Institute
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.Institute;

    public class BuildingRepository : PlataformaEducativaDbContextRepositoryBase<Building>
    {
        public BuildingRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<Building> All()
        {
            return this.Context.Building;
        }


        protected override Building MapNewValuesToOld(Building oldEntity, Building newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
