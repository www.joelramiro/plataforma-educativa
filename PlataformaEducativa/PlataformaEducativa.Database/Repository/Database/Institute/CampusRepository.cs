﻿// <copyright file="CampusRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.Institute
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.Institute;

    public class CampusRepository : PlataformaEducativaDbContextRepositoryBase<Campus>
    {
        public CampusRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<Campus> All()
        {
            return this.Context.Campus;
        }


        protected override Campus MapNewValuesToOld(Campus oldEntity, Campus newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
