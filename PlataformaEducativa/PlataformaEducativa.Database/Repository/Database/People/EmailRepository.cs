﻿// <copyright file="EmailRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.People;

    public class EmailRepository : PlataformaEducativaDbContextRepositoryBase<Email>
    {
        public EmailRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<Email> All()
        {
            return this.Context.Email;
        }


        protected override Email MapNewValuesToOld(Email oldEntity, Email newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
