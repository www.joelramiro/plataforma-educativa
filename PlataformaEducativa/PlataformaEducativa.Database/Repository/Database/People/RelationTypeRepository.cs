﻿// <copyright file="RelationTypeRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.People;

    public class RelationTypeRepository : PlataformaEducativaDbContextRepositoryBase<RelationType>
    {
        public RelationTypeRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<RelationType> All()
        {
            return this.Context.RelationType;
        }


        protected override RelationType MapNewValuesToOld(RelationType oldEntity, RelationType newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
