﻿// <copyright file="PeopleAddressRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.People;

    public class PeopleAddressRepository : PlataformaEducativaDbContextRepositoryBase<PeopleAddress>
    {
        public PeopleAddressRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<PeopleAddress> All()
        {
            return this.Context.PeopleAddress;
        }


        protected override PeopleAddress MapNewValuesToOld(PeopleAddress oldEntity, PeopleAddress newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
