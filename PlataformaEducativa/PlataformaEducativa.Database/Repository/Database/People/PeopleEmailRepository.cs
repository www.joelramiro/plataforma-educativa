﻿// <copyright file="PeopleEmailRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.People;

    public class PeopleEmailRepository : PlataformaEducativaDbContextRepositoryBase<PeopleEmail>
    {
        public PeopleEmailRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<PeopleEmail> All()
        {
            return this.Context.PeopleEmail;
        }


        protected override PeopleEmail MapNewValuesToOld(PeopleEmail oldEntity, PeopleEmail newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
