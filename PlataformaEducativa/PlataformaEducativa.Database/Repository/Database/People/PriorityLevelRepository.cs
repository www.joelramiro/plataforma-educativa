﻿// <copyright file="PriorityLevelRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.People;

    public class PriorityLevelRepository : PlataformaEducativaDbContextRepositoryBase<PriorityLevel>
    {
        public PriorityLevelRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<PriorityLevel> All()
        {
            return this.Context.PriorityLevel;
        }


        protected override PriorityLevel MapNewValuesToOld(PriorityLevel oldEntity, PriorityLevel newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
