﻿// <copyright file="BloodTypeRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.People;

    public class BloodTypeRepository : PlataformaEducativaDbContextRepositoryBase<BloodType>
    {
        public BloodTypeRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<BloodType> All()
        {
            return this.Context.BloodType;
        }


        protected override BloodType MapNewValuesToOld(BloodType oldEntity, BloodType newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
