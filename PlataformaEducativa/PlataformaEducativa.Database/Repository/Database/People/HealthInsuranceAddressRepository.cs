﻿// <copyright file="HealthInsuranceAddressRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.People;

    public class HealthInsuranceAddressRepository : PlataformaEducativaDbContextRepositoryBase<HealthInsuranceAddress>
    {
        public HealthInsuranceAddressRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<HealthInsuranceAddress> All()
        {
            return this.Context.HealthInsuranceAddress;
        }


        protected override HealthInsuranceAddress MapNewValuesToOld(HealthInsuranceAddress oldEntity, HealthInsuranceAddress newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
