﻿// <copyright file="EducationLevelRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.People;

    public class EducationLevelRepository : PlataformaEducativaDbContextRepositoryBase<EducationLevel>
    {
        public EducationLevelRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<EducationLevel> All()
        {
            return this.Context.EducationLevel;
        }


        protected override EducationLevel MapNewValuesToOld(EducationLevel oldEntity, EducationLevel newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
