﻿// <copyright file="DiseaseRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.People;

    public class DiseaseRepository : PlataformaEducativaDbContextRepositoryBase<Disease>
    {
        public DiseaseRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<Disease> All()
        {
            return this.Context.Disease;
        }


        protected override Disease MapNewValuesToOld(Disease oldEntity, Disease newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
