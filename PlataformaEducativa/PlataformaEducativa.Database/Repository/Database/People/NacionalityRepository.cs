﻿// <copyright file="NacionalityRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.People;

    public class NacionalityRepository : PlataformaEducativaDbContextRepositoryBase<Nacionality>
    {
        public NacionalityRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<Nacionality> All()
        {
            return this.Context.Nacionality;
        }


        protected override Nacionality MapNewValuesToOld(Nacionality oldEntity, Nacionality newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
