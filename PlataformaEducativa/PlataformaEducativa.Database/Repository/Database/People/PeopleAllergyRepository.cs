﻿// <copyright file="PeopleAllergyRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.People;

    public class PeopleAllergyRepository : PlataformaEducativaDbContextRepositoryBase<PeopleAllergy>
    {
        public PeopleAllergyRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<PeopleAllergy> All()
        {
            return this.Context.PeopleAllergy;
        }


        protected override PeopleAllergy MapNewValuesToOld(PeopleAllergy oldEntity, PeopleAllergy newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
