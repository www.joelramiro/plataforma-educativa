﻿// <copyright file="AddressRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.People;

    public class AddressRepository : PlataformaEducativaDbContextRepositoryBase<Address>
    {
        public AddressRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<Address> All()
        {
            return this.Context.Address;
        }


        protected override Address MapNewValuesToOld(Address oldEntity, Address newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
