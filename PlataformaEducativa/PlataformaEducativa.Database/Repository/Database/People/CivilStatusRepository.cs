﻿// <copyright file="CivilStatusRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.People;

    public class CivilStatusRepository : PlataformaEducativaDbContextRepositoryBase<CivilStatus>
    {
        public CivilStatusRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<CivilStatus> All()
        {
            return this.Context.CivilStatus;
        }


        protected override CivilStatus MapNewValuesToOld(CivilStatus oldEntity, CivilStatus newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
