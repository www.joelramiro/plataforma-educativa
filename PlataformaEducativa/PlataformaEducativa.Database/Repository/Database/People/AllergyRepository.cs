﻿// <copyright file="AllergyRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.People;

    public class AllergyRepository : PlataformaEducativaDbContextRepositoryBase<Allergy>
    {
        public AllergyRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<Allergy> All()
        {
            return this.Context.Allergy;
        }


        protected override Allergy MapNewValuesToOld(Allergy oldEntity, Allergy newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
