﻿// <copyright file="HealthInsuranceEmailRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.People;

    public class HealthInsuranceEmailRepository : PlataformaEducativaDbContextRepositoryBase<HealthInsuranceEmail>
    {
        public HealthInsuranceEmailRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<HealthInsuranceEmail> All()
        {
            return this.Context.HealthInsuranceEmail;
        }


        protected override HealthInsuranceEmail MapNewValuesToOld(HealthInsuranceEmail oldEntity, HealthInsuranceEmail newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
