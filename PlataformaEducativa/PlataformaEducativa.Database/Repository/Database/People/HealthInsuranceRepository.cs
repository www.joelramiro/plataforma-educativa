﻿// <copyright file="HealthInsuranceRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.People;

    public class HealthInsuranceRepository : PlataformaEducativaDbContextRepositoryBase<HealthInsurance>
    {
        public HealthInsuranceRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<HealthInsurance> All()
        {
            return this.Context.HealthInsurance;
        }


        protected override HealthInsurance MapNewValuesToOld(HealthInsurance oldEntity, HealthInsurance newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
