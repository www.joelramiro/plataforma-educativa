﻿// <copyright file="SexTypeRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.People;

    public class SexTypeRepository : PlataformaEducativaDbContextRepositoryBase<SexType>
    {
        public SexTypeRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<SexType> All()
        {
            return this.Context.SexType;
        }


        protected override SexType MapNewValuesToOld(SexType oldEntity, SexType newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
