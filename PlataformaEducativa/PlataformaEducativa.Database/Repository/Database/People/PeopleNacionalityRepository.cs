﻿// <copyright file="PeopleNacionalityRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.People;

    public class PeopleNacionalityRepository : PlataformaEducativaDbContextRepositoryBase<PeopleNacionality>
    {
        public PeopleNacionalityRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<PeopleNacionality> All()
        {
            return this.Context.PeopleNacionality;
        }


        protected override PeopleNacionality MapNewValuesToOld(PeopleNacionality oldEntity, PeopleNacionality newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
