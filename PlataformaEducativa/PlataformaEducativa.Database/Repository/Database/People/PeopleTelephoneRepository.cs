﻿// <copyright file="PeopleTelephoneRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.People;

    public class PeopleTelephoneRepository : PlataformaEducativaDbContextRepositoryBase<PeopleTelephone>
    {
        public PeopleTelephoneRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<PeopleTelephone> All()
        {
            return this.Context.PeopleTelephone;
        }


        protected override PeopleTelephone MapNewValuesToOld(PeopleTelephone oldEntity, PeopleTelephone newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
