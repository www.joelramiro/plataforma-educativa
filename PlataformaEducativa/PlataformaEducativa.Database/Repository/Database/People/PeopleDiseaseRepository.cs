﻿// <copyright file="PeopleDiseaseRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.People;

    public class PeopleDiseaseRepository : PlataformaEducativaDbContextRepositoryBase<PeopleDisease>
    {
        public PeopleDiseaseRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<PeopleDisease> All()
        {
            return this.Context.PeopleDisease;
        }


        protected override PeopleDisease MapNewValuesToOld(PeopleDisease oldEntity, PeopleDisease newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
