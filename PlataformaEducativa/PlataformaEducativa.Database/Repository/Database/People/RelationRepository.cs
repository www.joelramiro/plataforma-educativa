﻿// <copyright file="RelationRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.People;

    public class RelationRepository : PlataformaEducativaDbContextRepositoryBase<Relation>
    {
        public RelationRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<Relation> All()
        {
            return this.Context.Relation;
        }


        protected override Relation MapNewValuesToOld(Relation oldEntity, Relation newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
