﻿// <copyright file="HealthInsuranceTelephoneRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.People;

    public class HealthInsuranceTelephoneRepository : PlataformaEducativaDbContextRepositoryBase<HealthInsuranceTelephone>
    {
        public HealthInsuranceTelephoneRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<HealthInsuranceTelephone> All()
        {
            return this.Context.HealthInsuranceTelephone;
        }


        protected override HealthInsuranceTelephone MapNewValuesToOld(HealthInsuranceTelephone oldEntity, HealthInsuranceTelephone newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
