﻿// <copyright file="DisabilityRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.People;

    public class DisabilityRepository : PlataformaEducativaDbContextRepositoryBase<Disability>
    {
        public DisabilityRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<Disability> All()
        {
            return this.Context.Disability;
        }


        protected override Disability MapNewValuesToOld(Disability oldEntity, Disability newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
