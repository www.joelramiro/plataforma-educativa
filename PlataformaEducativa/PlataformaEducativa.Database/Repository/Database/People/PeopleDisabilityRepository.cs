﻿// <copyright file="PeopleDisabilityRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.People;

    public class PeopleDisabilityRepository : PlataformaEducativaDbContextRepositoryBase<PeopleDisability>
    {
        public PeopleDisabilityRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<PeopleDisability> All()
        {
            return this.Context.PeopleDisability;
        }


        protected override PeopleDisability MapNewValuesToOld(PeopleDisability oldEntity, PeopleDisability newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
