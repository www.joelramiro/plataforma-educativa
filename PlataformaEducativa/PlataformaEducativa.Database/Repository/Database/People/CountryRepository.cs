﻿// <copyright file="CountryRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;
    using PlataformaEducativa.Database.Model.People;

    public class CountryRepository : PlataformaEducativaDbContextRepositoryBase<Country>
    {
        public CountryRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<Country> All()
        {
            return this.Context.Country;
        }


        protected override Country MapNewValuesToOld(Country oldEntity, Country newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
