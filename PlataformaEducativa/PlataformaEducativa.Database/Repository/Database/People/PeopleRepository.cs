﻿// <copyright file="PeopleRepository.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Repository.Database.People
{
    using System;
    using System.Linq;
    using PlataformaEducativa.Database.Context;

    public class PeopleRepository : PlataformaEducativaDbContextRepositoryBase<Model.People.People>
    {
        public PeopleRepository(PlataformaEducativaContext context)
            : base(context)
        {
        }

        public override IQueryable<Model.People.People> All()
        {
            return this.Context.People;
        }


        protected override Model.People.People MapNewValuesToOld(Model.People.People oldEntity, Model.People.People newEntity)
        {
            throw new NotImplementedException();
        }
    }
}
