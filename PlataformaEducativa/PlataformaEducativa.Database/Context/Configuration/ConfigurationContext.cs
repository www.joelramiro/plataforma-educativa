﻿// <copyright file="ConfigurationContext.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Context.Configuration
{
    using System.Threading;
    using System.Threading.Tasks;
    using EfCore.Triggers.Runner;
    using Microsoft.EntityFrameworkCore;

    public class ConfigurationContext : DbContext
    {
        private readonly ITriggerRunner triggerRunner;

        public ConfigurationContext(
            DbContextOptions<ConfigurationContext> options,
            ITriggerRunner triggerRunner)
            : base(options)
        {
            this.triggerRunner = triggerRunner;
        }

        public override int SaveChanges()
        {
            return this.SaveChanges(true);
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            return this.triggerRunner.SaveChanges(this.ChangeTracker, () => base.SaveChanges(acceptAllChangesOnSuccess));
        }

        public Task<int> SaveChangesAsync()
        {
            return this.SaveChangesAsync(cancellationToken: default(CancellationToken));
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            return this.SaveChangesAsync(acceptAllChangesOnSuccess: true, cancellationToken: cancellationToken);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            return this.triggerRunner.SaveChangesAsync(this.ChangeTracker, (ct) => base.SaveChangesAsync(acceptAllChangesOnSuccess, ct), cancellationToken);
        }
    }
}
