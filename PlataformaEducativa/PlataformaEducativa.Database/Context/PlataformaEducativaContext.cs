﻿// <copyright file="PlataformaEducativaContext.cs" company="Open Source">
// Copyright (c) Open Source. All rights reserved.
// </copyright>

namespace PlataformaEducativa.Database.Context
{
    using System.Threading;
    using System.Threading.Tasks;
    using EfCore.Triggers.Runner;
    using Microsoft.EntityFrameworkCore;
    using PlataformaEducativa.Database.Model.Institute;
    using PlataformaEducativa.Database.Model.People;
    using PlataformaEducativa.Database.Model.Registration;

    public class PlataformaEducativaContext : DbContext
    {
        private readonly ITriggerRunner triggerRunner;

        public PlataformaEducativaContext(
            DbContextOptions<PlataformaEducativaContext> options,
            ITriggerRunner triggerRunner)
            : base(options)
        {
            this.triggerRunner = triggerRunner;
        }

        public DbSet<Address> Address { get; set; }

        public DbSet<Allergy> Allergy { get; set; }

        public DbSet<BloodType> BloodType { get; set; }

        public DbSet<CivilStatus> CivilStatus { get; set; }

        public DbSet<Country> Country { get; set; }

        public DbSet<Disability> Disability { get; set; }

        public DbSet<Disease> Disease { get; set; }

        public DbSet<EducationLevel> EducationLevel { get; set; }

        public DbSet<Email> Email { get; set; }

        public DbSet<HealthInsurance> HealthInsurance { get; set; }

        public DbSet<HealthInsuranceAddress> HealthInsuranceAddress { get; set; }

        public DbSet<HealthInsuranceEmail> HealthInsuranceEmail { get; set; }

        public DbSet<HealthInsuranceTelephone> HealthInsuranceTelephone { get; set; }

        public DbSet<Nacionality> Nacionality { get; set; }

        public DbSet<People> People { get; set; }

        public DbSet<PeopleAddress> PeopleAddress { get; set; }

        public DbSet<PeopleAllergy> PeopleAllergy { get; set; }

        public DbSet<PeopleDisability> PeopleDisability { get; set; }

        public DbSet<PeopleDisease> PeopleDisease { get; set; }

        public DbSet<PeopleEmail> PeopleEmail { get; set; }

        public DbSet<PeopleNacionality> PeopleNacionality { get; set; }

        public DbSet<PeopleTelephone> PeopleTelephone { get; set; }

        public DbSet<PriorityLevel> PriorityLevel { get; set; }

        public DbSet<Relation> Relation { get; set; }

        public DbSet<RelationType> RelationType { get; set; }

        public DbSet<SexType> SexType { get; set; }

        public DbSet<Telephone> Telephone { get; set; }

        public DbSet<Building> Building { get; set; }

        public DbSet<Campus> Campus { get; set; }

        public DbSet<Classroom> Classroom { get; set; }

        public DbSet<Institution> Institution { get; set; }

        public DbSet<Career> Career { get; set; }

        public DbSet<CareerCampus> CareerCampus { get; set; }

        public DbSet<CareerClass> CareerClass { get; set; }

        public DbSet<CareerStudent> CareerStudent { get; set; }

        public DbSet<Class> Class { get; set; }

        public DbSet<Period> Period { get; set; }

        public DbSet<Registration> Registration { get; set; }

        public DbSet<RegistrationDetail> RegistrationDetail { get; set; }

        public DbSet<Schedule> Schedule { get; set; }

        public DbSet<SectionClass> SectionClass { get; set; }

        public DbSet<Student> Student { get; set; }

        public DbSet<Teacher> Teacher { get; set; }

        public override int SaveChanges()
        {
            return this.SaveChanges(true);
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            return this.triggerRunner.SaveChanges(this.ChangeTracker, () => base.SaveChanges(acceptAllChangesOnSuccess));
        }

        public Task<int> SaveChangesAsync()
        {
            return this.SaveChangesAsync(cancellationToken: default(CancellationToken));
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            return this.SaveChangesAsync(acceptAllChangesOnSuccess: true, cancellationToken: cancellationToken);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            return this.triggerRunner.SaveChangesAsync(this.ChangeTracker, (ct) => base.SaveChangesAsync(acceptAllChangesOnSuccess, ct), cancellationToken);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<RegistrationDetail>()
                .HasOne(rd => rd.SectionClass)
                .WithMany(sc => sc.RegistrationDetails)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Relation>()
                .HasOne(rd => rd.People1)
                .WithMany(sc => sc.Relations)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
